package com.example.cicd;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        Pane mainPane = loadFXML("first.fxml");
        Scene mainScene = new Scene(mainPane);
        stage.setScene(mainScene);
        stage.setTitle("Main Window demo");

        // Find the button in the main window
        Button mainButton = (Button) mainPane.lookup("#mainButton");

        stage.show();
    }

    private Pane loadFXML(String fxmlPath) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlPath));
            return fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static void main(String[] args) {
        launch();
    }
}