package com.example.cicd;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }

    @FXML
    protected void onButtonClick() throws IOException {
        // Load the next window FXML
        //Pane nextPane = loadFXML("second.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("teste.fxml"));
        Pane nextPane = fxmlLoader.load();

        // Create and configure the scene for the next window
        Scene nextScene = new Scene(nextPane);
        Stage nextStage = new Stage();
        nextStage.setScene(nextScene);
        nextStage.setTitle("Next Window");

        // Show the next window
        nextStage.show();
    }
}