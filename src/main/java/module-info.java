module com.example.cicd {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;

    opens com.example.cicd to javafx.fxml;
    exports com.example.cicd;
}